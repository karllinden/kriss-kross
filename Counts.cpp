/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <algorithm>

#include "Counts.hpp"

using std::size_t;
using std::vector;

namespace {

using kriss_kross::Counts;

class Comparator {

public:
  Comparator(const vector<size_t>& a, const vector<size_t>& b)
    : a_(a)
    , b_(b)
    , index_(0)
    , last_(std::min(a.size(), b.size()))
  {
    //
  }

  Counts::Comparison compare() noexcept {
    if (a_.size() == b_.size()) {
      return compare_any();
    } else if (a_.size() < b_.size()) {
      return compare_less();
    } else {
      return compare_greater();
    }
  }

private:
  Counts::Comparison compare_any() noexcept {
    skip([](auto a, auto b) { return a == b; });
    return is_done()
      ? Counts::EQUAL
      : compare_not_equal();
  }

  Counts::Comparison compare_not_equal() noexcept {
    bool less = a_[index_] < b_[index_];
    ++index_;
    return less
      ? compare_less()
      : compare_greater();
  }

  Counts::Comparison compare_less() noexcept {
    skip([](auto a, auto b) { return a <= b; });
    return is_done()
      ? Counts::LESS
      : Counts::INCOMPARABLE;
  }

  Counts::Comparison compare_greater() noexcept {
    skip([](auto a, auto b) { return a >= b; });
    return is_done()
      ? Counts::GREATER
      : Counts::INCOMPARABLE;
  }

  template<typename BinaryPredicate>
  void skip(BinaryPredicate predicate) noexcept {
    while (index_ < last_ && predicate(a_[index_], b_[index_])) {
      ++index_;
    }
  }

  bool is_done() {
    return index_ == last_;
  }

private:
  const vector<size_t>& a_;
  const vector<size_t>& b_;
  size_t index_;
  size_t last_;

};

}

namespace kriss_kross {

Counts::Counts(vector<size_t> counts)
  : counts_(std::move(counts))
{
  canonicalize();
}

size_t Counts::max_non_zero_length() const noexcept {
  size_t size = counts_.size();
  return size != 0
    ? size - 1
    : 0;
}

size_t Counts::operator[](size_t length) const noexcept {
  return length < counts_.size()
    ? counts_[length]
    : 0;
}

Counts::Comparison Counts::compare_to(
    const Counts& that) const noexcept {
  Comparator cmp(counts_, that.counts_);
  return cmp.compare();
}

bool Counts::try_subtract(const Counts& that) {
  bool can_subtract = that <= *this;
  if (can_subtract) {
    subtract(that);
  }
  return can_subtract;
}

void Counts::canonicalize() {
  while (!counts_.empty() && counts_.back() == 0) {
    counts_.pop_back();
  }
}

void Counts::subtract(const Counts& that) {
  for (size_t i = 0; i < that.counts_.size(); ++i) {
    counts_[i] -= that.counts_[i];
  }
  canonicalize();
}

bool operator==(const Counts& a, const Counts& b) noexcept {
  return a.counts_ == b.counts_;
}

bool operator<(const Counts& a, const Counts& b) noexcept {
  return a.compare_to(b) == Counts::LESS;
}

bool operator>(const Counts& a, const Counts& b) noexcept {
  return a.compare_to(b) == Counts::GREATER;
}

bool operator<=(const Counts& a, const Counts& b) noexcept {
  Counts::Comparison cmp = a.compare_to(b);
  return cmp == Counts::LESS || cmp == Counts::EQUAL;
}

bool operator>=(const Counts& a, const Counts& b) noexcept {
  Counts::Comparison cmp = a.compare_to(b);
  return cmp == Counts::GREATER || cmp == Counts::EQUAL;
}

}
