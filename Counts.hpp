/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef COUNTS_HPP
# define COUNTS_HPP

# include <boost/operators.hpp>
# include <vector>

namespace kriss_kross {

class Counts
  : boost::equality_comparable<Counts> {

  friend bool operator==(const Counts&, const Counts&) noexcept;

public:
  enum Comparison {
    LESS,
    GREATER,
    EQUAL,
    INCOMPARABLE,
  };

  Counts() = default;
  Counts(std::vector<size_t> counts);

  // Returns the length of the maximum non-zero count.
  std::size_t max_non_zero_length() const noexcept;

  std::size_t operator[](std::size_t length) const noexcept;

  Comparison compare_to(const Counts& that) const noexcept;

  bool try_subtract(const Counts& that);

private:
  void canonicalize();
  void subtract(const Counts& that);

private:
  std::vector<size_t> counts_;

};

bool operator<(const Counts&, const Counts&) noexcept;
bool operator>(const Counts&, const Counts&) noexcept;
bool operator<=(const Counts&, const Counts&) noexcept;
bool operator>=(const Counts&, const Counts&) noexcept;

}

#endif /* !COUNTS_HPP */
