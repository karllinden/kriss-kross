/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <cstdlib>

#include "Direction.hpp"

namespace kriss_kross {

Direction::Direction(Value value) noexcept
    : value_(value)
{
  check_value();
}

Direction::Value Direction::value() const noexcept {
  return value_;
}

Direction Direction::other() const noexcept {
  return value_ == DOWN ? RIGHT : DOWN;
}

const Vector& Direction::to_vector() const noexcept {
  return value_ == DOWN
    ? Vector::down()
    : Vector::right();
}

Direction::operator Value() const noexcept {
  return value_;
}

bool Direction::operator==(const Direction& that) const noexcept {
  return value_ == that.value_;
}

void Direction::check_value() const noexcept {
  switch (value_) {
    case DOWN:
    case RIGHT:
      return;
  }

  std::abort();
}

}
