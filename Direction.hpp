/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef DIRECTION_HPP
# define DIRECTION_HPP

# include "Vector.hpp"

namespace kriss_kross {

class Direction
    : boost::equality_comparable<Direction>
{

public:
  enum Value {
    DOWN,
    RIGHT,
  };

  Direction(Value value) noexcept;

  Value value() const noexcept;
  Direction other() const noexcept;
  const Vector& to_vector() const noexcept;

  operator Value() const noexcept;
  bool operator==(const Direction& that) const noexcept;

private:
  void check_value() const noexcept;

private:
  Value value_;

};

}

#endif /* !DIRECTION_HPP */
