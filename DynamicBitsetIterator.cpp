/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include "DynamicBitsetIterator.hpp"

using std::size_t;

namespace kriss_kross {

DynamicBitsetIterator::DynamicBitsetIterator(const bitset* bitset,
    size_t index) noexcept
  : bitset_(bitset)
  , index_(index)
{
  //
}

size_t DynamicBitsetIterator::operator*() const noexcept {
  return index_;
}

bool DynamicBitsetIterator::operator==(
    const DynamicBitsetIterator& that) const noexcept {
  return index_ == that.index_;
}

DynamicBitsetIterator& DynamicBitsetIterator::operator++() noexcept {
  index_ = bitset_->find_next(index_);
  return *this;
}

DynamicBitsetIterator make_dynamic_bitset_begin(
    const boost::dynamic_bitset<>& bitset) noexcept {
  size_t index = bitset.find_first();
  return DynamicBitsetIterator(&bitset, index);
}

DynamicBitsetIterator make_dynamic_bitset_end() noexcept {
  return DynamicBitsetIterator(nullptr, boost::dynamic_bitset<>::npos);
}

}
