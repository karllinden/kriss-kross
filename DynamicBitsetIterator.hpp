/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef DYNAMIC_BITSET_ITERATOR_HPP
# define DYNAMIC_BITSET_ITERATOR_HPP

# include <boost/dynamic_bitset.hpp>
# include <boost/operators.hpp>

namespace kriss_kross {

class DynamicBitsetIterator
  : boost::equality_comparable<DynamicBitsetIterator>
  , boost::incrementable<DynamicBitsetIterator> {

  using bitset = boost::dynamic_bitset<>;

  friend DynamicBitsetIterator make_dynamic_bitset_begin(
      const bitset&) noexcept;
  friend DynamicBitsetIterator make_dynamic_bitset_end() noexcept;

public:
  using value_type = std::size_t;
  using difference_type = std::size_t;
  using pointer = value_type*;
  using reference = value_type&;
  using iterator_category = std::input_iterator_tag;

private:
  DynamicBitsetIterator(const bitset* bitset,
      std::size_t index) noexcept;

public:
  std::size_t operator*() const noexcept;
  bool operator==(const DynamicBitsetIterator& that) const noexcept;
  DynamicBitsetIterator& operator++() noexcept;

private:
  const bitset* bitset_;
  std::size_t index_;

};

DynamicBitsetIterator make_dynamic_bitset_begin(
    const boost::dynamic_bitset<>& bitset) noexcept;
DynamicBitsetIterator make_dynamic_bitset_end() noexcept;

}

#endif /* !DYNAMIC_BITSET_ITERATOR_HPP */
