#include <algorithm>
#include <cassert>
#include <exception>
#include <iostream>
#include <memory>

#include <boost/iterator/transform_iterator.hpp>

#include "DynamicBitsetIterator.hpp"
#include "Problem.hpp"
#include "SolutionSet.hpp"

using std::make_unique;
using std::size_t;
using std::string;
using std::unique_ptr;
using std::vector;

namespace kriss_kross {

namespace ForwardSolve {

enum Status {
  CONTRADICTION,
  NO_PROGRESS,
  PROGRESS,
};

template<typename Function>
Status loop_while_progress(Function function) {
  Status status = NO_PROGRESS;
  while (true) {
    Status s = function();
    switch (s) {
      case CONTRADICTION:
        return CONTRADICTION;
      case NO_PROGRESS:
        return status;
      case PROGRESS:
        status = PROGRESS;
        break;
    }
  }
}

template<typename Iterator, typename Function>
Status forward_solve_each(Iterator it, Iterator end,
    Function function) {
  Status status = NO_PROGRESS;

  for (; it != end; ++it) {
    Status s = function(*it);
    switch (s) {
      case CONTRADICTION:
        return CONTRADICTION;
      case NO_PROGRESS:
        break;
      case PROGRESS:
        status = PROGRESS;
        break;
    }
  }

  return status;
}

}

class GuessStack;
class Solver;

class Guess {

public:
  Guess() noexcept;
  Guess(const Place& guess, size_t n_words) noexcept;

  const Place* place() const noexcept;

  bool is_better_than(const Guess& that) const noexcept;

private:
  const Place* place_;
  size_t n_words_;

};

class Graph {

private:
  struct ToPlace {

    using result_type = const Place&;

    explicit ToPlace(const Graph& host) noexcept
      : host_(&host)
    {
      //
    }

    const Place& operator()(size_t index) const noexcept {
      return host_->places_[index];
    }

  private:
    const Graph* host_;

  };

public:
  Graph(const Problem& problem, size_t length);

  ForwardSolve::Status forward_solve(Solver& solver) const;

  Guess find_best_guess() const;

  void fill_place(const Place& place, const Word& word);
  void remove_non_fitting_words(const Place& place,
      const RectCharMap& char_map);
  void guess_at(const Place& place, const Solver& solver,
      GuessStack& stack) const;

private:
  ForwardSolve::Status forward_solve_places(Solver& solver) const;
  ForwardSolve::Status forward_solve_place(const Place& place,
      Solver& solver) const;
  void do_forward_solve_place(const Place& place, Solver& solver) const;

  size_t count_words(const Place& place) const;

  void close_place(const Place& place);
  void remove_word(const Word& word);

  using EdgesIterator = vector<size_t>::iterator;
  using ConstEdgesIterator = vector<size_t>::const_iterator;
  EdgesIterator edges_begin(const Place& place) noexcept;
  EdgesIterator edges_end(const Place& place) noexcept;
  ConstEdgesIterator edges_begin(const Place& place) const noexcept;
  ConstEdgesIterator edges_end(const Place& place) const noexcept;

  using OpenPlacesIterator =
    boost::transform_iterator<ToPlace, DynamicBitsetIterator>;
  OpenPlacesIterator open_places_begin() const noexcept;
  OpenPlacesIterator open_places_end() const noexcept;

  vector<size_t> create_edges(const RectCharMap& char_map) const;

  bool is_place_open(const Place& place) const noexcept;

  const Word& to_word(ConstEdgesIterator iter) const noexcept;

  size_t to_edge_index(const Place& place,
      const Word& word) const noexcept;
  size_t begin_index(const Place& place) const noexcept;
  size_t end_index(const Place& place) const noexcept;

private:
  Places::WithLength places_;
  Words::WithLength words_;
  boost::dynamic_bitset<> open_places_;
  vector<size_t> edges_;

};

vector<Graph> create_graphs(const Problem& problem);

class Solver {

public:
  explicit Solver(const Problem& problem);

  void fill_place(const Place& place, const Word& word);
  void solve(SolutionSet& solutions, GuessStack& stack);

private:
  ForwardSolve::Status forward_solve();
  ForwardSolve::Status forward_solve_graphs();

  void remove_non_fitting_words(const Place& place);
  Graph& get_graph(const Place& place);
  Graph& get_graph(size_t length);
  const Place* find_best_place_to_guess() const noexcept;
  void guess_at(const Place& place, GuessStack& stack);

private:
  vector<Graph> graphs_;
  RectCharMap char_map_;

};

class GuessStack {

public:
  void push(unique_ptr<Solver>&& guess);
  void execute(SolutionSet& solutions);

private:
  unique_ptr<Solver> pop();

private:
  std::stack<unique_ptr<Solver>> guesses_;

};

Guess::Guess() noexcept
  : place_(nullptr)
  , n_words_(std::numeric_limits<size_t>::max())
{
  //
}

Guess::Guess(const Place& place, size_t n_words) noexcept
  : place_(&place)
  , n_words_(n_words)
{
  assert(n_words_ > 1);
}

const Place* Guess::place() const noexcept {
  return place_;
}

bool Guess::is_better_than(const Guess& that) const noexcept {
  if (n_words_ < that.n_words_) {
    return true;
  } else if (n_words_ > that.n_words_) {
    return false;
  } else if (place_ != nullptr) {
    assert(that.place_ != nullptr);
    return place_->is_better_guess_than(*that.place_);
  } else {
    assert(that.place_ == nullptr);
    return false;
  }
}

Graph::Graph(const Problem& problem, size_t length)
  : places_(problem.layout().places_with_length(length))
  , words_(problem.word_map().words_with_length(length))
  , open_places_(places_.size())
  , edges_(create_edges(problem.layout().char_map()))
{
  open_places_.set();
}

ForwardSolve::Status Graph::forward_solve(Solver& solver) const {
  return ForwardSolve::loop_while_progress(
      [&]() { return forward_solve_places(solver); });
}

Guess Graph::find_best_guess() const {
  Guess best_guess;

  auto end = open_places_end();
  for (auto it = open_places_begin(); it != end; ++it) {
    const Place& place = *it;

    Guess guess(place, count_words(place));
    if (guess.is_better_than(best_guess)) {
      best_guess = guess;
    }
  }

  return best_guess;
}

size_t Graph::count_words(const Place& place) const {
  size_t result = 0;
  auto end = edges_end(place);
  for (auto it = edges_begin(place); it != end; ++it) {
    result += (*it > 0);
  }
  return result;
}

void Graph::fill_place(const Place& place, const Word& word) {
  close_place(place);
  remove_word(word);
  edges_[to_edge_index(place, word)] = 1;
}

void Graph::remove_non_fitting_words(const Place& place,
    const RectCharMap& char_map) {
  if (!is_place_open(place)) {
    return;
  }

  string str = char_map.get(place.segment());

  auto end = edges_end(place);
  for (auto it = edges_begin(place); it != end; ++it) {
    if (*it > 0 && !to_word(it).fits(str)) {
      *it = 0;
    }
  }
}

void Graph::guess_at(const Place& place, const Solver& solver,
    GuessStack& stack) const {
  vector<const Word*> words;

  auto end = edges_end(place);
  for (auto it = edges_begin(place); it != end; ++it) {
    if (*it > 0) {
      const Word& word = to_word(it);
      words.push_back(&word);
    }
  }

  for (auto it = words.rbegin(); it != words.rend(); ++it) {
    unique_ptr<Solver> guess = make_unique<Solver>(solver);
    guess->fill_place(place, **it);
    stack.push(std::move(guess));
  }
}

ForwardSolve::Status Graph::forward_solve_places(Solver& solver) const {
  return ForwardSolve::forward_solve_each(
      open_places_begin(),
      open_places_end(),
      [&](const Place& p) { return forward_solve_place(p, solver); });
}

ForwardSolve::Status Graph::forward_solve_place(const Place& place,
    Solver& solver) const {
  using namespace ForwardSolve;

  size_t count = count_words(place);
  switch (count) {
    case 0:
      return CONTRADICTION;
    case 1:
      do_forward_solve_place(place, solver);
      return PROGRESS;
    default:
      return NO_PROGRESS;
  }
}

void Graph::do_forward_solve_place(const Place& place,
    Solver& solver) const {
  auto end = edges_end(place);
  for (auto it = edges_begin(place); it != end; ++it) {
    if (*it > 0) {
      solver.fill_place(place, to_word(it));
    }
  }
}

void Graph::close_place(const Place& place) {
  auto end = edges_end(place);
  for (auto it = edges_begin(place); it != end; ++it) {
    *it = 0;
  }
  open_places_.reset(place.length_index());
}

void Graph::remove_word(const Word& word) {
  auto end = open_places_end();
  for (auto it = open_places_begin(); it != end; ++it) {
    size_t& edge = edges_[to_edge_index(*it, word)];
    edge -= (edge > 0);
  }
}

Graph::EdgesIterator Graph::edges_begin(const Place& place) noexcept {
  return edges_.begin() + begin_index(place);
}

Graph::EdgesIterator Graph::edges_end(const Place& place) noexcept {
  return edges_begin(place) + words_.size();
}

Graph::ConstEdgesIterator Graph::edges_begin(
    const Place& place) const noexcept {
  return edges_.begin() + begin_index(place);
}

Graph::ConstEdgesIterator Graph::edges_end(
    const Place& place) const noexcept {
  return edges_begin(place) + words_.size();
}

Graph::OpenPlacesIterator Graph::open_places_begin() const noexcept {
  return OpenPlacesIterator(
      make_dynamic_bitset_begin(open_places_),
      ToPlace(*this));
}

Graph::OpenPlacesIterator Graph::open_places_end() const noexcept {
  return OpenPlacesIterator(
      make_dynamic_bitset_end(),
      ToPlace(*this));
}

vector<size_t> Graph::create_edges(const RectCharMap& char_map) const {
  vector<size_t> result(places_.size() * words_.size());
  for (const Place& place : places_) {
    string str = char_map.get(place.segment());
    for (const Word& word : words_) {
      size_t index = to_edge_index(place, word);
      if (word.fits(str)) {
        result[index] = word.count();
      }
    }
  }
  return result;
}

bool Graph::is_place_open(const Place& place) const noexcept {
  return open_places_.test(place.length_index());
}

const Word& Graph::to_word(ConstEdgesIterator iter) const noexcept {
  size_t index = std::distance(edges_.begin(), iter);
  return words_[index % words_.size()];
}

size_t Graph::to_edge_index(const Place& place,
    const Word& word) const noexcept {
  return begin_index(place) + word.length_index();
}

size_t Graph::begin_index(const Place& place) const noexcept {
  return place.length_index() * words_.size();
}

size_t Graph::end_index(const Place& place) const noexcept {
  return (place.length_index() + 1) * words_.size();
}

vector<Graph> create_graphs(const Problem& problem) {
  size_t max_length = problem.layout().max_place_length();
  vector<Graph> result;
  result.reserve(max_length + 1);
  for (size_t length = 0; length <= max_length; ++length) {
    result.emplace_back(problem, length);
  }
  return result;
}

Solver::Solver(const Problem& problem)
  : graphs_(create_graphs(problem))
  , char_map_(problem.layout().char_map())
{
  //
}

void Solver::fill_place(const Place& place, const Word& word) {
  if (!word.is_unknown()) {
    char_map_.put(place.segment(), word.string());
  }
  get_graph(place).fill_place(place, word);
  remove_non_fitting_words(place);
}

void Solver::solve(SolutionSet& solutions, GuessStack& stack) {
  ForwardSolve::Status fss = forward_solve();
  if (fss == ForwardSolve::CONTRADICTION) {
    return;
  }

  const Place* place = find_best_place_to_guess();
  if (place == nullptr) {
    solutions.add(std::move(char_map_));
  } else {
    guess_at(*place, stack);
  }
}

ForwardSolve::Status Solver::forward_solve() {
  return ForwardSolve::loop_while_progress(
      [this]() { return forward_solve_graphs(); });
}

ForwardSolve::Status Solver::forward_solve_graphs() {
  return ForwardSolve::forward_solve_each(
      graphs_.begin(),
      graphs_.end(),
      [this](Graph& graph) { return graph.forward_solve(*this); });
}

void Solver::remove_non_fitting_words(const Place& place) {
  for (const Place* neighbor : place.neighbors()) {
    get_graph(*neighbor)
      .remove_non_fitting_words(*neighbor, char_map_);
  }
}

Graph& Solver::get_graph(const Place& place) {
  return get_graph(place.length());
}

Graph& Solver::get_graph(size_t length) {
  if (length >= graphs_.size()) {
    throw std::runtime_error("Invalid length");
  }
  return graphs_[length];
}

const Place* Solver::find_best_place_to_guess() const noexcept {
  Guess best_guess;

  for (const Graph& graph : graphs_) {
    Guess guess = graph.find_best_guess();
    if (guess.is_better_than(best_guess)) {
      best_guess = guess;
    }
  }

  return best_guess.place();
}

void Solver::guess_at(const Place& place, GuessStack& stack) {
  get_graph(place).guess_at(place, *this, stack);
}

void GuessStack::push(unique_ptr<Solver>&& guess) {
  guesses_.push(std::move(guess));
}

void GuessStack::execute(SolutionSet& solutions) {
  while (!guesses_.empty() && !solutions.is_full()) {
    pop()->solve(solutions, *this);
  }
}

unique_ptr<Solver> GuessStack::pop() {
  unique_ptr<Solver> top = std::move(guesses_.top());
  guesses_.pop();
  return top;
}

static SolutionSet solve(const Problem& problem) {
  SolutionSet result;
  GuessStack stack;

  Solver solver(problem);
  solver.solve(result, stack);
  stack.execute(result);

  return result;
}

}

int main(int argc, const char *argv[]) {
  using namespace kriss_kross;

  if (argc != 3) {
    std::cerr
      << "Usage: kriss-kross <LAYOUT_FILE> <WORDS_FILE>"
      << std::endl;
    return EXIT_FAILURE;
  }

  try {
    Problem problem = Problem::read(argv[1], argv[2]);
    SolutionSet solutions = solve(problem);
    std::cout << solutions;
    return EXIT_SUCCESS;
  } catch (InvalidWordsException& ex) {
    std::cerr << ex.what();
    return EXIT_FAILURE;
  } catch (const std::exception& ex) {
    std::cerr << ex.what() << std::endl;
    return EXIT_FAILURE;
  }
}
