/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <algorithm>

#include "Layout.hpp"

using std::size_t;
using std::vector;

namespace {

using namespace kriss_kross;

struct PlacesComputer {

public:
  PlacesComputer(const RectCharMap& char_map)
    : char_map_(char_map)
  {
    //
  }

  void compute_places() {
    result_.reserve(char_map_.rect().size() * 2);
    compute_places_in_direction(Direction::DOWN);
    compute_places_in_direction(Direction::RIGHT);
  }

  vector<Place> result() noexcept {
    vector<Place> result;
    using std::swap;
    swap(result_, result);
    return result;
  }

private:
  void compute_places_in_direction(Direction dir) {
    const Vector& vec = dir.to_vector();
    const Vector& other_vec = dir.other().to_vector();

    const Rect& rect = char_map_.rect();
    Rect::Iter i1 = rect.iter(rect.top_left(), other_vec);

    const Point *begin;
    while ((begin = i1.next()) != nullptr) {
      Rect::Iter i2 = rect.iter(*begin, vec);

      while (i2.has_next()) {
        find_next_place(dir, i2);
      }
    }
  }

  void find_next_place(Direction dir, Rect::Iter& iter) {
    const Point* pt;
    char ch;

    while ((pt = iter.next()) != nullptr
        && (ch = getchar(*pt)) == ' ') {
      //
    }

    if (pt == nullptr) {
      return;
    }

    /*
     * Because pt != nullptr, we know that ch != ' '. Thus, we might be
     * at the beginning of a place (if there is at least another
     * character in this row/column.
     */
    Point start = *pt;
    size_t len = 1;

    while ((pt = iter.next()) != nullptr
        && (ch = getchar(*pt)) != ' ') {
      len++;
    }

    if (len >= 2) {
      result_.emplace_back(start, dir, len);
    }
  }

  char getchar(const Point& pt) const noexcept {
    return char_map_.get(pt);
  }

private:
  const RectCharMap& char_map_;
  vector<Place> result_;

};

vector<Place> compute_places(const RectCharMap& char_map) {
  PlacesComputer computer(char_map);
  computer.compute_places();
  return computer.result();
}

}

namespace kriss_kross {

Interval::Interval(int begin, int end)
  : begin_(begin)
  , end_(end)
{
    //
}

bool Interval::contains(int value) const noexcept {
  return begin_ <= value && value < end_;
}

const Direction DownPlace::DIRECTION = Direction::DOWN;
const Direction RightPlace::DIRECTION = Direction::RIGHT;

DownPlace::DownPlace(const Point& start, size_t length)
  : x(start.x())
  , y(start.y(), start.y() + length)
{
  //
}

RightPlace::RightPlace(const Point& start, size_t length)
  : x(start.x(), start.x() + length)
  , y(start.y())
{
  //
}

static std::variant<DownPlace, RightPlace> create_place_variant(
    const Point& start, Direction direction, size_t length) {
  switch (direction) {
    case Direction::DOWN:
      return DownPlace(start, length);
    case Direction::RIGHT:
      return RightPlace(start, length);
    default:
      throw std::runtime_error("Invalid Direction");
  }
}

Place::Place(const Point& start, Direction direction, size_t length)
  : variant_(create_place_variant(start, direction, length))
  , segment_(start, direction, length)
{
  //
}

static const struct {

  typedef Direction result_type;

  template<typename P>
  Direction operator()(const P&) const noexcept {
    return P::DIRECTION;
  }

} direction_visitor;

Direction Place::direction() const noexcept {
  return std::visit(direction_visitor, variant_);
}

const Segment& Place::segment() const noexcept {
  return segment_;
}

const std::vector<const Place*>& Place::neighbors() const noexcept {
  return neighbors_;
}

size_t Place::length() const noexcept {
  return segment_.length();
}

size_t Place::length_index() const noexcept {
  return length_index_;
}

void Place::set_length_index(std::size_t index) noexcept {
  length_index_ = index;
}

template<typename... Props>
bool is_greater(const Place& left, const Place& right, Props... props);

template<>
bool is_greater(const Place& left, const Place& right) {
  return false;
}

template<typename Prop, typename... Props>
bool is_greater(const Place& left, const Place& right,
    Prop prop, Props... props) {
  auto left_value = prop(left);
  auto right_value = prop(right);
  if (left_value > right_value) {
    return true;
  } else if (left_value < right_value) {
    return false;
  } else {
    return is_greater(left, right, props...);
  }
}

bool Place::is_better_guess_than(const Place& that) const noexcept{
  return is_greater(*this, that,
      [](const Place& p) { return p.neighbors().size(); },
      [](const Place& p) { return p.length(); },
      [](const Place& p) { return p.segment_.start().x(); },
      [](const Place& p) { return p.segment_.start().y(); },
      [](const Place& p) { return p.direction().value(); });
}

bool Place::operator<(const Place& that) const noexcept {
  return !is_greater(*this, that,
      [](const Place& p) { return p.length(); },
      [](const Place& p) { return p.direction().value(); },
      [](const Place& p) { return p.segment_.start().x(); },
      [](const Place& p) { return p.segment_.start().y(); });
}

void Place::compute_neighbors(const Places& places) {
  neighbors_.reserve(places.size());

  for (const Place& place : places) {
    if (intersects(place)) {
      neighbors_.push_back(&place);
    }
  }

  neighbors_.shrink_to_fit();
}

static const struct {

  typedef bool result_type;

  bool operator()(const RightPlace& right,
      const DownPlace& down) const {
    return right.x.contains(down.x) && down.y.contains(right.y);
  }

  bool operator()(const DownPlace& down,
      const RightPlace& right) const {
    return (*this)(right, down);
  }

  bool operator()(const DownPlace&, const DownPlace&) const {
    return false;
  }

  bool operator()(const RightPlace&, const RightPlace&) const {
    return false;
  }

} intersects_visitor;

bool Place::intersects(const Place& that) const noexcept {
  return std::visit(intersects_visitor, variant_, that.variant_);
}

Layout::Layout(RectCharMap char_map)
  : char_map_(std::move(char_map))
  , places_(compute_places(char_map_))
{
  compute_neighbors();
}

Layout Layout::read(const std::string& filename) {
  return Layout(RectCharMap::read(filename));
}

void Layout::compute_neighbors() {
  for (Place& place : places_) {
    place.compute_neighbors(places_);
  }
}

size_t Layout::max_place_length() const {
  return places_.max_length();
}

const Places& Layout::places() const {
  return places_;
}

Counts Layout::count_places() const {
  vector<size_t> counts(places_.max_length() + 1);
  for (size_t i = 0; i <= places_.max_length(); ++i) {
    counts[i] = places_.with_length(i).size();
  }
  return counts;
}

Places::WithLength Layout::places_with_length(
    size_t length) const noexcept {
  return places_.with_length(length);
}

const RectCharMap& Layout::char_map() const noexcept {
  return char_map_;
}

}
