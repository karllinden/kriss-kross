/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef LAYOUT_HPP
# define LAYOUT_HPP

# include <variant>

# include "Counts.hpp"
# include "Direction.hpp"
# include "RectCharMap.hpp"
# include "SortedObjects.hpp"

namespace kriss_kross {

class Interval {

public:
  Interval(int begin, int end);

  bool contains(int value) const noexcept;

private:
  int begin_;
  int end_;

};

class DownPlace {

public:
  static const Direction DIRECTION;

  DownPlace(const Point& start, std::size_t length);

  int x;
  Interval y;

};

class RightPlace {

public:
  static const Direction DIRECTION;

  RightPlace(const Point& start, std::size_t length);

  Interval x;
  int y;

};

class Place;
using Places = SortedObjects<Place>;

class Place : public boost::less_than_comparable<Place> {

public:
  Place(const Point& start, Direction direction, std::size_t length);

  Direction direction() const noexcept;
  const Segment& segment() const noexcept;
  const std::vector<const Place*>& neighbors() const noexcept;
  std::size_t length() const noexcept;

  std::size_t length_index() const noexcept;
  void set_length_index(std::size_t index) noexcept;

  bool is_better_guess_than(const Place& that) const noexcept;

  bool operator<(const Place& that) const noexcept;

  void compute_neighbors(const Places& places_);

private:
  bool intersects(const Place& that) const noexcept;

private:
  std::variant<DownPlace, RightPlace> variant_;
  Segment segment_;
  std::size_t length_index_;
  std::vector<const Place*> neighbors_;

};

class Layout {

public:
  explicit Layout(RectCharMap char_map);

  static Layout read(const std::string& filename);

  std::size_t max_place_length() const;

  const Places& places() const;
  Counts count_places() const;
  Places::WithLength places_with_length(
      std::size_t length) const noexcept;

  const RectCharMap& char_map() const noexcept;

private:
  void compute_neighbors();

private:
  RectCharMap char_map_;
  Places places_;

};

}

#endif /* !LAYOUT_HPP */
