/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include "Line.hpp"
#include "ReadFile.hpp"

namespace kriss_kross {

Line::Line(const Lines& lines, std::string& str, unsigned number)
  : lines_(&lines)
  , number_(number)
{
  std::swap(string_, str);
}

const std::string& Line::filename() const noexcept {
  return lines_->filename();
}

Lines::Lines(std::string&& filename, std::vector<std::string>& lines)
  : filename_(std::move(filename))
{
  vector_.reserve(lines.size());

  unsigned number = 1;
  for (std::string& str : lines) {
    vector_.emplace_back(*this, str, number);
    ++number;
  }
}

std::unique_ptr<Lines> Lines::read(std::string filename) {
  std::vector<std::string> lines = read_file(filename);
  return std::make_unique<Lines>(std::move(filename), lines);
}

std::unique_ptr<Lines> Lines::from_vector(
    std::vector<std::string> vector) {
  return std::make_unique<Lines>("<unknown>", vector);
}

}
