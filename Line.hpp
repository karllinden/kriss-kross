/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef LINE_HPP
# define LINE_HPP

# include <memory>
# include <string>
# include <vector>

namespace kriss_kross {

class Lines;

class Line final {

  friend class Lines;

public:
  Line(const Lines& lines, std::string& str, unsigned number);

  const std::string& filename() const noexcept;
  const std::string& string() const noexcept { return string_; }
  unsigned number() const noexcept { return number_; }

private:
  const Lines* lines_;
  std::string string_;
  unsigned number_;

};

class Lines final {

public:
  Lines(std::string&& filename, std::vector<std::string>& lines);

  Lines(const Lines&) = delete;
  Lines& operator=(const Lines&) = delete;
  Lines(Lines&&) = delete;
  Lines& operator=(Lines&&) = delete;

  static std::unique_ptr<Lines> read(std::string filename);
  static std::unique_ptr<Lines> from_vector(
      std::vector<std::string> vector);

  const std::string& filename() const noexcept { return filename_; }

  typedef std::vector<Line>::const_iterator iterator;
  iterator begin() const noexcept { return vector_.begin(); }
  iterator end() const noexcept { return vector_.end(); }

private:
  void read_from_file();

private:
  std::string filename_;
  std::vector<Line> vector_;

};

}

#endif /* !LINE_HPP */
