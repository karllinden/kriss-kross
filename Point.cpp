/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <utility>

#include "Point.hpp"

namespace kriss_kross {

Point::Point() noexcept
    : Point(0, 0)
{
    //
}

Point::Point(int x, int y) noexcept
    : pos_(x, y)
{
    //
}

Point::Point(Vector&& position) noexcept
    : pos_(std::move(position))
{
    //
}

Point& Point::operator+=(const Vector& that) noexcept {
    pos_ += that;
    return *this;
}

Vector Point::operator-(const Point& that) const noexcept {
    return pos_ - that.pos_;
}

bool Point::operator==(const Point& that) const noexcept {
    return pos_ == that.pos_;
}

bool Point::operator<=(const Point& that) const noexcept {
    return pos_ <= that.pos_;
}

bool Point::operator<(const Point& that) const noexcept {
    return pos_ < that.pos_;
}

const Vector& Point::position() const noexcept {
    return pos_;
}

int Point::x() const noexcept {
    return pos_.x;
}

int Point::y() const noexcept {
    return pos_.y;
}

Point min(const Point& p, const Point& q) noexcept{
    return min(p.position(), q.position());
}

Point max(const Point& p, const Point& q) noexcept{
    return max(p.position(), q.position());
}

}
