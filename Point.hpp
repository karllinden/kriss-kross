/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef POINT_HPP
# define POINT_HPP

# include "Vector.hpp"

namespace kriss_kross {

class Point
    : boost::addable<Point, Vector>
    , boost::equality_comparable<Point>
{

public:
    Point() noexcept;
    Point(int x, int y) noexcept;
    Point(Vector&& position) noexcept;

    Point& operator+=(const Vector& that) noexcept;
    Vector operator-(const Point& that) const noexcept;

    bool operator==(const Point& that) const noexcept;

    bool operator<=(const Point& that) const noexcept;
    bool operator<(const Point& that) const noexcept;

    const Vector& position() const noexcept;
    int x() const noexcept;
    int y() const noexcept;

private:
    Vector pos_;

};

Point min(const Point& p, const Point& q) noexcept;
Point max(const Point& p, const Point& q) noexcept;

}

#endif /* !POINT_HPP */
