/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <sstream>

#include "Problem.hpp"

using std::size_t;

namespace kriss_kross {

static std::string create_exception_message(size_t len,
    size_t n_places, size_t n_words) {
  std::ostringstream oss;
  oss
    << "There are more words ("
    << n_words
    << ") with "
    << len
    << " letters than there are places ("
    << n_places
    << ").";
  return oss.str();
}

MoreWordsThanPlaces::MoreWordsThanPlaces(size_t len,
    size_t n_places, size_t n_words)
  : len(len)
  , n_places(n_places)
  , n_words(n_words)
  , what_(create_exception_message(len, n_places, n_words))
{
  //
}

const char* MoreWordsThanPlaces::what() const noexcept {
  return what_.c_str();
}

Problem::Problem(Layout&& layout, const WordMap& word_map)
  : layout_(std::move(layout))
  , word_map_(with_unknowns(word_map))
{
  //
}

Problem Problem::read(const std::string& layout_filename,
    const std::string& words_filename) {
  return {
    Layout::read(layout_filename),
    WordMap::read(words_filename)
  };
}

const Layout& Problem::layout() const {
  return layout_;
}

const WordMap& Problem::word_map() const {
  return word_map_;
}

WordMap Problem::with_unknowns(const WordMap& word_map) const {
  Counts unknown_counts = compute_unknown_counts(word_map);
  return WordMap::combine(word_map, WordMap::unknowns(unknown_counts));
}

Counts Problem::compute_unknown_counts(const WordMap& word_map) const {
  Counts result = layout_.count_places();
  Counts word_counts = word_map.count_words();

  if (!result.try_subtract(word_counts)) {
    throw_more_words_than_places(result, word_counts);
  }

  return result;
}

void Problem::throw_more_words_than_places(const Counts& place_counts,
    const Counts& word_counts) const {
  for (size_t len = 0; ; ++len) {
    size_t n_places = place_counts[len];
    size_t n_words = word_counts[len];
    if (n_words > n_places) {
      throw MoreWordsThanPlaces(len, n_places, n_words);
    }
  }
}

}
