/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef PROBLEM_HPP
# define PROBLEM_HPP

# include "Layout.hpp"
# include "Word.hpp"

namespace kriss_kross {

class MoreWordsThanPlaces : public std::exception {

public:
  MoreWordsThanPlaces(std::size_t len, std::size_t n_places,
      std::size_t n_words);

  const char* what() const noexcept override;

  std::size_t len;
  std::size_t n_places;
  std::size_t n_words;

private:
  std::string what_;

};

class Problem {

private:
  Problem(Layout&& layout, const WordMap& word_map);

public:
  static Problem read(const std::string& layout_filename,
      const std::string& words_filename);

  const Layout& layout() const;
  const WordMap& word_map() const;

private:
  WordMap with_unknowns(const WordMap& word_map) const;
  Counts compute_unknown_counts(const WordMap& word_map) const;
  void throw_more_words_than_places(const Counts& place_counts,
      const Counts& word_counts) const;

private:
  Layout layout_;
  WordMap word_map_;

};

}

#endif /* !PROBLEM_HPP */
