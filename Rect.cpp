/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <algorithm>

#include "Rect.hpp"

namespace kriss_kross {

static const Vector zero_vector(0, 0);

Rect::Rect() noexcept
  : top_left_(0, 0)
  , span_(0, 0)
{
  //
}

Rect::Rect(const Point& p1, const Point& p2) noexcept
  : top_left_(min(p1, p2))
  , span_(max(p1, p2) - top_left_)
{
  //
}

bool Rect::operator==(const Rect& that) const noexcept {
  return top_left_ == that.top_left_
    && span_ == that.span_;
}

const Point& Rect::top_left() const noexcept {
  return top_left_;
}

bool Rect::contains(const Point& pt) const noexcept {
  return contains(relativize(pt));
}

bool Rect::contains(const Vector& pos) const noexcept {
  return zero_vector <= pos && pos < span_;
}

unsigned Rect::size() const noexcept {
  return span_.x * span_.y;
}

unsigned Rect::width() const noexcept {
  return span_.x;
}

int Rect::index_of(const Point& pt) const noexcept {
  Vector pos = relativize(pt);
  return contains(pos)
    ? pos.y * width() + pos.x
    : -1;
}

Vector Rect::relativize(const Point& pt) const noexcept {
  return pt - top_left_;
}

}
