/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef RECT_HPP
# define RECT_HPP

# include <array>

# include "Point.hpp"

namespace kriss_kross {

class Rect
  : boost::equality_comparable<Rect>
{

public:
  Rect() noexcept;
  Rect(const Point& p1, const Point& p2) noexcept;

  bool operator==(const Rect& that) const noexcept;

  const Point& top_left() const noexcept;

  bool contains(const Point& pt) const noexcept;
private:
  bool contains(const Vector& pos) const noexcept;

public:
  unsigned size() const noexcept;
  unsigned width() const noexcept;
  int index_of(const Point& pt) const noexcept;

  // Not very idiomatic, I know...
  class Iter {

    friend class Rect;

    Iter(const Rect* rect, const Point& start, const Vector& vec)
      : rect_(rect)
      , vec_(vec)
      , next_(0)
    {
      pts_[0] = start;
    }

  public:
    bool has_next() const {
      return rect_->contains(pts_[next_]);
    }

    const Point* next() {
      if (!has_next()) {
        return nullptr;
      }

      const Point* result = &pts_[next_];
      pts_[!next_] = *result + vec_;
      next_ = !next_;

      return result;
    }

  private:
    const Rect* rect_;
    Vector vec_;
    unsigned next_;
    std::array<Point, 2> pts_;

  };

  Iter iter(const Point& start, const Vector& vec) const {
    return Iter(this, start, vec);
  }

private:
  Vector relativize(const Point& pt) const noexcept;

private:
  Point top_left_;
  Vector span_;

};

}

#endif /* !RECT_HPP */
