/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include "ReadFile.hpp"
#include "RectCharMap.hpp"

namespace {

using namespace kriss_kross;

void remove_trailing_empty_lines(std::vector<std::string>& lines) {
  while (lines.back().empty()) {
    lines.pop_back();
  }
}

Point compute_bottom_right(
    const std::vector<std::string>& lines) noexcept {
  std::size_t rows = 0;
  std::size_t cols = 0;

  for (const std::string& line : lines) {
    rows++;
    cols = std::max(cols, line.size());
  }

  return {static_cast<int>(cols), static_cast<int>(rows)};
}

Rect compute_rect(const std::vector<std::string>& lines) noexcept {
  return Rect(Point(0, 0), compute_bottom_right(lines));
}

}

namespace kriss_kross {

RectCharMap::RectCharMap() {
  //
}

RectCharMap::RectCharMap(const Rect& rect, char fill)
  : rect_(rect)
  , vector_(rect.size(), fill)
{
  //
}

RectCharMap RectCharMap::read(const std::string& filename) {
  std::vector<std::string> lines = read_file(filename);
  remove_trailing_empty_lines(lines);
  return from_lines(lines);
}

RectCharMap RectCharMap::from_lines(
    const std::vector<std::string>& lines) {
  RectCharMap result(compute_rect(lines), ' ');
  result.fill_with(lines);
  return result;
}

char RectCharMap::get(const Point& pt) const {
  return vector_[rect_.index_of(pt)];
}

std::string RectCharMap::get(const Segment& segment) const {
  unsigned len = segment.length();

  std::string result;
  result.reserve(len);

  Rect::Iter iter = rect_.iter(segment.start(), segment.vector());

  const Point* pt;
  while ((pt = iter.next()) != nullptr && len--) {
    result.push_back(get(*pt));
  }

  return result;
}

void RectCharMap::put(const Segment& segment, const std::string& str) {
  Rect::Iter rit = rect_.iter(segment.start(), segment.vector());

  const Point* pt;
  unsigned len = segment.length();
  for (auto sit = str.begin();
      sit != str.end() && (pt = rit.next()) != nullptr && len;
      ++sit, --len) {
    put(*pt, *sit);
  }
}

const Rect& RectCharMap::rect() const {
  return rect_;
}

void RectCharMap::put(const Point& pt, char ch) {
  vector_[rect_.index_of(pt)] = ch;
}

void RectCharMap::fill_with(const std::vector<std::string>& lines) {
  Segment segment(rect_.top_left(), Direction::RIGHT, rect_.width());
  for (const std::string& line : lines) {
    put(segment, line);
    segment += Vector::down();
  }
}

}
