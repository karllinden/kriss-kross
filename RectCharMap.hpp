/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef RECT_CHAR_MAP_HPP
# define RECT_CHAR_MAP_HPP

# include <ostream>
# include <string>
# include <vector>

# include "Rect.hpp"
# include "Segment.hpp"

namespace kriss_kross {

class RectCharMap {

public:
  RectCharMap();
  RectCharMap(const Rect& rect, char fill);

  static RectCharMap read(const std::string& filename);

private:
  static RectCharMap from_lines(const std::vector<std::string>& lines);

public:
  char get(const Point& pt) const;
  std::string get(const Segment& segment) const;
  void put(const Segment& segment, const std::string& str);

  const Rect& rect() const;

private:
  void put(const Point& pt, char ch);
  void fill_with(const std::vector<std::string>& lines);

private:
  Rect rect_;
  std::vector<char> vector_;

};

template<typename Stream>
Stream& operator<<(Stream& stream, const RectCharMap& map) {
    const Rect& rect = map.rect();

    Rect::Iter rows = rect.iter(rect.top_left(), Vector::down());

    const Point* row;
    while ((row = rows.next()) != nullptr) {
        Rect::Iter pts = rect.iter(*row, Vector::right());

        const Point* pt;
        while ((pt = pts.next()) != nullptr) {
            stream << map.get(*pt);
        }

        stream << std::endl;
    }

    return stream;
}

}

#endif /* !RECT_CHAR_MAP_HPP */
