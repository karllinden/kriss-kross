/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include "Segment.hpp"

namespace kriss_kross {

Segment::Segment(const Point& start, Direction direction,
    std::size_t length) noexcept
  : start_(start)
  , direction_(direction)
  , length_(length)
{
  //
}

const Point& Segment::start() const noexcept {
  return start_;
}

Direction Segment::direction() const noexcept {
  return direction_;
}

const Vector& Segment::vector() const noexcept {
  return direction_.to_vector();
}

std::size_t Segment::length() const noexcept {
  return length_;
}

Segment& Segment::operator+=(const Vector& vector) noexcept {
  start_ += vector;
  return *this;
}

}
