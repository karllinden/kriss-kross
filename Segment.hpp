/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef SEGMENT_HPP
# define SEGMENT_HPP

# include "Direction.hpp"
# include "Point.hpp"

namespace kriss_kross {

class Segment
  : boost::addable<Vector>
{

public:
  Segment(const Point& start, Direction direction,
      std::size_t length) noexcept;

  const Point& start() const noexcept;
  Direction direction() const noexcept;
  const Vector& vector() const noexcept;
  std::size_t length() const noexcept;

  Segment& operator+=(const Vector& vector) noexcept;

private:
  Point start_;
  Direction direction_;
  std::size_t length_;

};

}

#endif /* !SEGMENT_HPP */
