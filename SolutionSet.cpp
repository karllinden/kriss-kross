/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include "SolutionSet.hpp"

using std::endl;
using std::ostream;

namespace kriss_kross {

SolutionSet::SolutionSet()
  : full_(false)
{
  solutions_.reserve(MAX_SOLUTIONS);
}

void SolutionSet::add(RectCharMap&& map) {
  if (solutions_.size() < MAX_SOLUTIONS) {
    solutions_.emplace_back(std::move(map));
  } else {
    full_ = true;
  }
}

bool SolutionSet::is_full() const {
  return full_;
}

void SolutionSet::print(ostream& os) const {
  if (solutions_.empty()) {
    print_empty(os);
  } else {
    print_non_empty(os);
  }
}

void SolutionSet::print_empty(ostream& os) const {
  os << "No solution." << endl;
}

void SolutionSet::print_non_empty(ostream& os) const {
  print_solution(os, 0);
  for (unsigned i = 1; i < solutions_.size(); ++i) {
    os << endl;
    print_solution(os, i);
  }
}

void SolutionSet::print_solution(ostream& os, unsigned index) const {
  const char *suffix = full_ ? "+" : "";
  os
    << "Solution "
    << index + 1
    << " of "
    << solutions_.size()
    << suffix
    << ':'
    << endl
    << solutions_[index];
}

ostream& operator<<(ostream& os, const SolutionSet& solutions) {
  solutions.print(os);
  return os;
}

}
