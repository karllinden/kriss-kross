/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef SOLUTION_SET_HPP
# define SOLUTION_SET_HPP

# include <stack>

# include "RectCharMap.hpp"

namespace kriss_kross {

class SolutionSet;

std::ostream& operator<<(std::ostream&, const SolutionSet&);

class SolutionSet {

  friend std::ostream& operator<<(std::ostream&, const SolutionSet&);

private:
  static constexpr std::size_t MAX_SOLUTIONS = 10;

public:
  SolutionSet();

  void add(RectCharMap&& map);
  bool is_full() const;

private:
  void print(std::ostream& os) const;
  void print_empty(std::ostream& os) const;
  void print_non_empty(std::ostream& os) const;
  void print_solution(std::ostream& os, unsigned index) const;

private:
  std::vector<RectCharMap> solutions_;
  bool full_;

};

}

#endif /* !SOLUTION_SET_HPP */
