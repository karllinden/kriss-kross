/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef SORTED_OBJECTS_HPP
# define SORTED_OBJECTS_HPP

# include <algorithm>
# include <vector>

namespace kriss_kross {

namespace detail {

template<typename T>
struct Compare {

  bool operator()(const T& a, const T& b) const {
    auto al = a.length();
    auto bl = b.length();
    if (al < bl) {
      return true;
    } else if (al > bl) {
      return false;
    } else {
      return a < b;
    }
  }

};

template<typename T>
std::vector<T> prepare_vector(std::vector<T>&& vec) {
  vec.shrink_to_fit();
  Compare<T> cmp;
  std::sort(vec.begin(), vec.end(), cmp);
  return std::move(vec);
}

template<typename T>
std::vector<size_t> compute_nonempty_begins(const std::vector<T>& vec) {
  std::vector<size_t> result;

  size_t max = vec.back().length();
  result.reserve(max + 1);

  size_t index = 0;
  while (result.size() <= max) {
    if (vec[index].length() < result.size()) {
      index++;
    } else {
      result.push_back(index);
    }
  }

  return result;
}

template<typename T>
std::vector<size_t> compute_begins(const std::vector<T>& vec) {
  if (!vec.empty()) {
    return compute_nonempty_begins(vec);
  } else {
    return {};
  }
}

}

template<typename T>
class SortedObjects {

public:
  SortedObjects(std::vector<T> vector)
    : vector_(detail::prepare_vector(std::move(vector)))
    , begins_(detail::compute_begins(vector_))
  {
    set_length_indices();
  }

  const std::vector<T>& vector() const noexcept {
    return vector_;
  }

  std::size_t size() const noexcept {
    return vector_.size();
  }

  std::size_t max_length() const noexcept {
    return vector_.empty() ? 0 : vector_.back().length();
  }

  using iterator = typename std::vector<T>::iterator;
  using const_iterator = typename std::vector<T>::const_iterator;

  iterator begin() noexcept {
    return vector_.begin();
  }

  iterator end() noexcept {
    return vector_.end();
  }

  const_iterator begin() const noexcept {
    return vector_.begin();
  }

  const_iterator end() const noexcept {
    return vector_.end();
  }

  class WithLength {

    friend class SortedObjects;

  private:
    WithLength(const SortedObjects& host, std::size_t length)
      : host_(&host)
      , length_(length)
    {
      //
    }

  public:
    std::size_t length() const noexcept {
      return length_;
    }

    std::size_t size() const noexcept {
      return std::distance(begin(), end());
    }

    const T& operator[](std::size_t index) const noexcept {
      return *(begin() + index);
    }

    const_iterator begin() const noexcept {
      return host_->length_begin(length_);
    }

    const_iterator end() const noexcept {
      return host_->length_begin(length_ + 1);
    }

  private:
    const SortedObjects* host_;
    std::size_t length_;

  };

  WithLength with_length(std::size_t length) const noexcept {
    return {*this, length};
  }

private:
  const_iterator length_begin(std::size_t length) const noexcept {
    if (length < begins_.size()) {
      return vector_.begin() + begins_[length];
    } else {
      return vector_.end();
    }
  }

  void set_length_indices() noexcept {
    std::size_t length = 0;
    std::size_t index = 0;
    for (T& object : vector_) {
      if (length != object.length()) {
        length = object.length();
        index = 0;
      }

      object.set_length_index(index);
      ++index;
    }
  }

private:
  std::vector<T> vector_;
  std::vector<std::size_t> begins_;

};

}

#endif /* !SORTED_OBJECTS_HPP */
