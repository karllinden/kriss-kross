/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <algorithm>

#include "Vector.hpp"

namespace kriss_kross {

static const Vector vector_down(0, 1);
static const Vector vector_right(1, 0);

const Vector& Vector::down() {
  return vector_down;
}

const Vector& Vector::right() {
   return vector_right;
}

Vector::Vector(int x, int y) noexcept
  : x(x)
  , y(y)
{
  //
}

Vector& Vector::operator+=(const Vector& that) noexcept {
  x += that.x;
  y += that.y;
  return *this;
}

Vector& Vector::operator-=(const Vector& that) noexcept {
  x -= that.x;
  y -= that.y;
  return *this;
}

bool Vector::operator==(const Vector& that) const noexcept {
  return x == that.x
    && y == that.y;
}

bool Vector::operator<=(const Vector& that) const noexcept {
  return x <= that.x
    && y <= that.y;
}

bool Vector::operator<(const Vector& that) const noexcept {
  return x < that.x
    && y < that.y;
}

Vector min(const Vector& u, const Vector& v) noexcept {
  return Vector(std::min(u.x, v.x), std::min(u.y, v.y));
}

Vector max(const Vector& u, const Vector& v) noexcept {
  return Vector(std::max(u.x, v.x), std::max(u.y, v.y));
}

}
