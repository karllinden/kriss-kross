/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <array>
#include <sstream>

#include "ReadFile.hpp"
#include "Word.hpp"

using std::ostream;
using std::size_t;
using std::string;
using std::vector;

namespace {

using namespace kriss_kross;

static constexpr std::array<char, 2> INVALID_LETTERS = {' ', '?'};

void validate_length(const string& str,
    vector<InvalidWord::Reason>& reasons) {
  if (str.size() == 1) {
    reasons.push_back(InvalidLength());
  }
}

void validate_letters(const string& str,
    vector<InvalidWord::Reason>& reasons) {
  for (char ch : INVALID_LETTERS) {
    auto pos = str.find(ch);
    if (pos != string::npos) {
      reasons.push_back(InvalidLetter(ch));
    }
  }
}

vector<InvalidWord::Reason> validate_known_word(const string& str) {
  vector<InvalidWord::Reason> reasons;
  validate_length(str, reasons);
  validate_letters(str, reasons);
  return reasons;
}

void validate_line(const Line& line, vector<InvalidWord>& result) {
  auto reasons = validate_known_word(line.string());
  if (!reasons.empty()) {
    result.emplace_back(line, std::move(reasons));
  }
}

vector<InvalidWord> validate_lines(const Lines& lines) {
  vector<InvalidWord> result;
  for (const Line& line : lines) {
    validate_line(line, result);
  }
  return result;
}

void throw_if_invalid_lines(const Lines& lines) {
  vector<InvalidWord> iws = validate_lines(lines);
  if (!iws.empty()) {
    throw InvalidWordsException(iws);
  }
}

vector<Word> to_unsorted_words(const Lines& lines) {
  vector<Word> result;
  for (const Line& line : lines) {
    const string& word = line.string();
    if (!word.empty()) {
      result.emplace_back(Word::known(word));
    }
  }
  return result;
}

vector<Word> without_duplicates(const vector<Word>& words) {
  vector<Word> result;
  result.reserve(words.size());
  for (const Word& word : words) {
    if (!result.empty()
        && result.back().string() == word.string()) {
      result.back().increment_count();
    } else {
      result.push_back(word);
    }
  }
  return result;
}

vector<Word> to_words(const Lines& lines) {
  throw_if_invalid_lines(lines);
  vector<Word> words = to_unsorted_words(lines);
  std::sort(words.begin(), words.end());
  return without_duplicates(words);
}

vector<Word> to_unknown_words(const Counts& counts) {
  vector<Word> words;
  size_t max = counts.max_non_zero_length();
  for (size_t len = 0; len <= max; ++len) {
    size_t count = counts[len];
    if (count != 0) {
      words.emplace_back(Word::unknown(len));
      words.back().add_count(count - 1);
    }
  }
  return words;
}

}

namespace kriss_kross {

InvalidLetter::InvalidLetter(char value) noexcept
  : value_(value)
{
  //
}

ostream& operator<<(ostream& os, const InvalidLetter& err) {
  os << "Invalid letter '" << err.value_ << "'";
  return os;
}

ostream& operator<<(ostream& os, const InvalidLength& err) {
  os << "Words must contain more than one letter";
  return os;
}

InvalidWord::InvalidWord(const Line& line,
    vector<Reason> reasons) noexcept
  : filename_(line.filename())
  , line_number_(line.number())
  , reasons_(std::move(reasons))
{
  //
}

class ReasonPrinter final {

public:
  typedef void result_type;

  explicit ReasonPrinter(ostream& os)
    : os_(os)
  {
    //
  }

  template<typename Error>
  void operator()(const Error& err) const {
    os_ << err;
  }

private:
  ostream& os_;

};

ostream& operator<<(ostream& os, const InvalidWord& iw) {
  for (const InvalidWord::Reason& reason : iw.reasons_) {
    os << iw.filename_ << ':' << iw.line_number_ << ": ";
    ReasonPrinter printer(os);
    std::visit(printer, reason);
    os << '\n';
  }
  return os;
}

InvalidWordsException::InvalidWordsException(
    const vector<InvalidWord>& invalid_words)
  : what_(create_message(invalid_words))
{
  //
}

const char* InvalidWordsException::what() const noexcept {
  return what_.c_str();
}

string InvalidWordsException::create_message(
    const vector<InvalidWord>& invalid_words) {
  std::ostringstream oss;
  for (const InvalidWord& iw : invalid_words) {
    oss << iw;
  }
  return oss.str();
}

Word::Word(std::string&& string) noexcept
  : string_(std::move(string))
  , count_(1)
{
  //
}

Word Word::known(std::string string) {
  auto reasons = validate_known_word(string);
  if (!reasons.empty()) {
    throw std::runtime_error("Invalid known word.");
  }
  return Word(std::move(string));
}

Word Word::unknown(size_t length) {
  if (length == 0) {
    throw std::runtime_error("Invalid unknown word length.");
  }
  return Word(std::string(length, '?'));
}

const string& Word::string() const noexcept {
  return string_;
}

size_t Word::length() const noexcept {
  return string_.length();
}

size_t Word::count() const noexcept {
  return count_;
}

void Word::add_count(size_t count) noexcept {
  count_ += count;
}

void Word::increment_count() noexcept {
  ++count_;
}

size_t Word::length_index() const noexcept {
  return length_index_;
}

void Word::set_length_index(std::size_t index) noexcept {
  length_index_ = index;
}

bool Word::fits(const std::string& str) const noexcept {
  return str.length() == string_.length()
    && (is_unknown() || characters_fit(str));
}

bool Word::is_unknown() const noexcept {
  return string_.length() > 0 && string_[0] == '?';
}

bool Word::characters_fit(const std::string& str) const noexcept {
  bool fits = true;
  for (size_t i = 0; i < string_.length(); ++i) {
    fits &= (str[i] == '?' || str[i] == string_[i]);
  }
  return fits;
}

bool Word::operator<(const Word& that) const noexcept {
  auto this_length = length();
  auto that_length = that.length();
  if (this_length < that_length) {
    return true;
  } else if (this_length > that_length) {
    return false;
  } else {
    return string_ < that.string_;
  }
}

WordMap::WordMap(const Lines& lines)
  : WordMap(to_words(lines))
{
  //
}

WordMap::WordMap(vector<Word>&& words)
  : words_(std::move(words))
{
  //
}

WordMap WordMap::read(const string& filename) {
  std::unique_ptr<Lines> lines = Lines::read(filename);
  return WordMap(*lines);
}

WordMap WordMap::from_known_words(vector<string> vector) {
  std::unique_ptr<Lines> lines = Lines::from_vector(std::move(vector));
  return WordMap(*lines);
}

WordMap WordMap::unknowns(const Counts& counts) {
  return WordMap(to_unknown_words(counts));
}

class WordMap::Combiner {

public:
  Combiner(const WordMap& first, const WordMap& second)
    : maps_({&first, &second})
  {
    //
  }

  WordMap combine() {
    return WordMap(combine_to_vector());
  }

  vector<Word> combine_to_vector() {
    size_t max_length = compute_max_length();

    for (length_ = 0; length_ <= max_length; ++length_) {
      combine_with_length();
    }

    vector<Word> result;
    std::swap(result_, result);
    return result;
  }

  size_t compute_max_length() const noexcept {
    size_t result = 0;
    for (size_t i = 0; i < maps_.size(); ++i) {
      result = std::max(result, maps_[i]->words_.max_length());
    }
    return result;
  }

  void combine_with_length() {
    initialize_iterators();
    initialize_ends();

    while (has_more<0>() && has_more<1>()) {
      if (*iterators_[0] < *iterators_[1]) {
        insert<0>();
      } else {
        insert<1>();
      }
    }

    flush_remaining<0>();
    flush_remaining<1>();
  }

  void initialize_iterators() {
    for (size_t i = 0; i < maps_.size(); ++i) {
      iterators_[i] = maps_[i]->words_with_length(length_).begin();
    }
  }

  void initialize_ends() {
    for (size_t i = 0; i < maps_.size(); ++i) {
      ends_[i] = maps_[i]->words_with_length(length_).end();
    }
  }

  template<size_t Index>
  void flush_remaining() {
    while (has_more<Index>()) {
      insert<Index>();
    }
  }

  template<size_t Index>
  bool has_more() const noexcept {
    return iterators_[Index] != ends_[Index];
  }

  template<size_t Index>
  void insert() {
    insert_pending<Index>();
    ++iterators_[Index];
  }

  template<size_t Index>
  void insert_pending() {
    const Word& pending = *iterators_[Index];
    if (!result_.empty()
        && result_.back().string() == pending.string()) {
      result_.back().add_count(pending.count());
    } else {
      result_.push_back(pending);
    }
  }

private:
  std::array<const WordMap*, 2> maps_;
  std::array<Words::const_iterator, 2> iterators_;
  std::array<Words::const_iterator, 2> ends_;
  vector<Word> result_;
  size_t length_;

};

WordMap WordMap::combine(const WordMap& first, const WordMap& second) {
  Combiner combiner(first, second);
  return combiner.combine();
}

Words::WithLength WordMap::words_with_length(
    size_t length) const noexcept {
  return words_.with_length(length);
}

Counts WordMap::count_words() const {
  vector<size_t> counts(words_.max_length() + 1);
  for (size_t i = 0; i <= words_.max_length(); ++i) {
    counts[i] = count_words_with_length(i);
  }
  return counts;
}

size_t WordMap::count_words_with_length(size_t length) const noexcept {
  size_t result = 0;
  for (const Word& w : words_.with_length(length)) {
    result += w.count();
  }
  return result;
}

}
