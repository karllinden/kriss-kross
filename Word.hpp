/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef WORD_HPP
# define WORD_HPP

# include <ostream>
# include <variant>

# include <boost/operators.hpp>

# include "Counts.hpp"
# include "Line.hpp"
# include "SortedObjects.hpp"

namespace kriss_kross {

class InvalidLetter final {

  friend std::ostream& operator<<(std::ostream&, const InvalidLetter&);

public:
  InvalidLetter(char value) noexcept;

private:
  char value_;

};

class InvalidLength final {

  friend std::ostream& operator<<(std::ostream&, const InvalidLength&);

};

class InvalidWord final {

  friend std::ostream& operator<<(std::ostream&, const InvalidWord&);

public:
  typedef std::variant<InvalidLetter, InvalidLength> Reason;

  InvalidWord(const Line& line, std::vector<Reason> reasons) noexcept;

private:
  std::string filename_;
  unsigned line_number_;
  std::vector<Reason> reasons_;

};

class InvalidWordsException final : public std::exception {

public:
  InvalidWordsException(const std::vector<InvalidWord>& invalid_words);

  const char* what() const noexcept override;

private:
  static std::string create_message(
      const std::vector<InvalidWord>& invalid_words);

private:
  std::string what_;

};

class Word : public boost::less_than_comparable<Word> {

private:
  explicit Word(std::string&& string) noexcept;

public:
  static Word known(std::string string);
  static Word unknown(std::size_t length);

  const std::string& string() const noexcept;
  std::size_t length() const noexcept;

  std::size_t count() const noexcept;
  void add_count(std::size_t count) noexcept;
  void increment_count() noexcept;

  std::size_t length_index() const noexcept;
  void set_length_index(std::size_t index) noexcept;

  bool fits(const std::string& str) const noexcept;
  bool is_unknown() const noexcept;

  bool operator<(const Word& that) const noexcept;

private:
  bool characters_fit(const std::string& str) const noexcept;

private:
  std::string string_;
  std::size_t count_;
  std::size_t length_index_;

};

using Words = SortedObjects<Word>;

class WordMap {

private:
  explicit WordMap(const Lines& lines);
  explicit WordMap(std::vector<Word>&& words);

public:
  static WordMap combine(const WordMap& first, const WordMap& second);
  static WordMap read(const std::string& filename);
  static WordMap from_known_words(std::vector<std::string> vector);
  static WordMap unknowns(const Counts& counts);

  Words::WithLength words_with_length(
      std::size_t length) const noexcept;

  Counts count_words() const;

private:
  std::size_t count_words_with_length(
      std::size_t length) const noexcept;

private:
  class Combiner;

private:
  Words words_;

};

}

#endif /* !WORD_HPP */
