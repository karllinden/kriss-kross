#!/bin/bash

if [ $# -ne 2 ]
then
    echo "Usage: test.sh EXECUTABLE TEST_NAME"
    exit 1
fi

die() {
    echo "die: $@" >&2
    exit 1
}

cd "$(realpath $(dirname $0))/tests/$2" || die "cd failed"

tmpdir=$(mktemp -d krisss-kross.XXXXXX --tmpdir)
$1 layout.txt words.txt 1>"$tmpdir"/stdout.txt 2>"$tmpdir"/stderr.txt

diff -u stdout.txt "$tmpdir"/stdout.txt || die "diff failed"
diff -u stderr.txt "$tmpdir"/stderr.txt || die "diff failed"

rm -rf "$tmpdir"
