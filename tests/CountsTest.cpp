/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <gtest/gtest.h>

#include "Counts.hpp"

using std::size_t;
using std::vector;

namespace kriss_kross {

vector<size_t> empty_vector() {
  return {};
}

void expect_equal(const Counts& a, const Counts& b) {
  EXPECT_EQ(Counts::EQUAL, a.compare_to(b));
  EXPECT_EQ(Counts::EQUAL, b.compare_to(a));
  EXPECT_FALSE(a < b);
  EXPECT_FALSE(b < a);
  EXPECT_FALSE(a > b);
  EXPECT_FALSE(b > a);
  EXPECT_TRUE(a <= b);
  EXPECT_TRUE(b <= a);
  EXPECT_TRUE(a >= b);
  EXPECT_TRUE(b >= a);
  EXPECT_TRUE(a == b);
  EXPECT_FALSE(a != b);
}

void expect_less(const Counts& a, const Counts& b) {
  EXPECT_EQ(Counts::LESS, a.compare_to(b));
  EXPECT_EQ(Counts::GREATER, b.compare_to(a));
  EXPECT_TRUE(a < b);
  EXPECT_FALSE(b < a);
  EXPECT_FALSE(a > b);
  EXPECT_TRUE(b > a);
  EXPECT_TRUE(a <= b);
  EXPECT_FALSE(b <= a);
  EXPECT_FALSE(a >= b);
  EXPECT_TRUE(b >= a);
  EXPECT_FALSE(a == b);
  EXPECT_TRUE(a != b);
}

void expect_incomparable(const Counts& a, const Counts& b) {
  EXPECT_EQ(Counts::INCOMPARABLE, a.compare_to(b));
  EXPECT_EQ(Counts::INCOMPARABLE, b.compare_to(a));
  EXPECT_FALSE(a < b);
  EXPECT_FALSE(b < a);
  EXPECT_FALSE(a > b);
  EXPECT_FALSE(b > a);
  EXPECT_FALSE(a <= b);
  EXPECT_FALSE(b <= a);
  EXPECT_FALSE(a >= b);
  EXPECT_FALSE(b >= a);
  EXPECT_FALSE(a == b);
  EXPECT_TRUE(a != b);
}

class CountsTest : public ::testing::Test {

protected:
  CountsTest()
    : empty_(empty_vector())
    , zeroes_({0, 0, 0})
  {
    //
  }

protected:
  Counts default_;
  Counts empty_;
  Counts zeroes_;

};

TEST_F(CountsTest, DefaultConstructorWorks) {
  EXPECT_EQ(0, default_[0]);
  EXPECT_EQ(0, default_[1]);
  EXPECT_EQ(0, default_[2]);
}

TEST_F(CountsTest, ConstructFromEmptyVectorWorks) {
  EXPECT_EQ(0, empty_[0]);
  EXPECT_EQ(0, empty_[1]);
  EXPECT_EQ(0, empty_[2]);
}

TEST_F(CountsTest, ConstructFromVectorWithZeroesWorks) {
  EXPECT_EQ(0, zeroes_[0]);
  EXPECT_EQ(0, zeroes_[1]);
  EXPECT_EQ(0, zeroes_[2]);
}

TEST_F(CountsTest, ConstructFromVectorWithNonZeroWorks) {
  Counts counts({3, 2, 5});
  EXPECT_EQ(3, counts[0]);
  EXPECT_EQ(2, counts[1]);
  EXPECT_EQ(5, counts[2]);
}

TEST_F(CountsTest, ZeroVectorAndEmptyGiveEqualCounts) {
  expect_equal(empty_, zeroes_);
}

TEST_F(CountsTest, DefaultEqualsZeroes) {
  expect_equal(default_, zeroes_);
}

TEST_F(CountsTest, DefaultEqualsEmpty) {
  expect_equal(default_, empty_);
}

TEST_F(CountsTest, EmptyNotEqualsNonZero) {
  Counts c2({0, 1, 0});
  ASSERT_NE(default_, c2);
}

TEST_F(CountsTest, ZeoresLessThanNonZeroes) {
  Counts zo({0, 1});
  expect_less(zeroes_, zo);
}

TEST_F(CountsTest, Compare123To234) {
  Counts c123({1, 2, 3});
  Counts c234({2, 3, 4});
  expect_less(c123, c234);
}

TEST_F(CountsTest, Compare01To02) {
  Counts c01({0, 1});
  Counts c02({0, 2});
  expect_less(c01, c02);
}

TEST_F(CountsTest, Compare01To011) {
  Counts c01({0, 1});
  Counts c011({0, 1, 1});
  expect_less(c01, c011);
}

TEST_F(CountsTest, Compare01To001) {
  Counts c01({0, 1});
  Counts c001({0, 0, 1});
  expect_incomparable(c01, c001);
}

TEST_F(CountsTest, 12TrySubtract21DoesNotSubtract) {
  Counts c12({1, 2});
  Counts expected(c12);
  Counts c21({2, 1});
  bool result = c12.try_subtract(c21);
  EXPECT_FALSE(result);
  EXPECT_EQ(expected, c12);
}

TEST_F(CountsTest, 12TrySubtract12ProducesDefault) {
  Counts c12({1, 2});
  bool result = c12.try_subtract(c12);
  EXPECT_TRUE(result);
  EXPECT_EQ(default_, c12);
}

TEST_F(CountsTest, 333TrySubtract23Produces103) {
  Counts c333({3, 3, 3});
  Counts c23({2, 3});
  Counts expected({1, 0, 3});
  bool result = c333.try_subtract(c23);
  EXPECT_TRUE(result);
  EXPECT_EQ(expected, c333);
}

}
