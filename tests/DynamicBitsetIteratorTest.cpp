/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <algorithm>

#include <gtest/gtest.h>

#include "DynamicBitsetIterator.hpp"

namespace kriss_kross {

class DynamicBitsetIteratorTest : public ::testing::Test {

protected:
  DynamicBitsetIteratorTest()
    : bitset_(10)
  {
    //
  }

  DynamicBitsetIterator make_begin() const noexcept {
    return make_dynamic_bitset_begin(bitset_);
  }

  DynamicBitsetIterator make_end() const noexcept {
    return make_dynamic_bitset_end();
  }

protected:
  boost::dynamic_bitset<> bitset_;

};

TEST_F(DynamicBitsetIteratorTest, MakeDynamicBitsetBeginWorks) {
  make_begin();
}

TEST_F(DynamicBitsetIteratorTest, BeginEqualsBegin) {
  ASSERT_EQ(make_begin(), make_begin());
}

TEST_F(DynamicBitsetIteratorTest, NotEqualsIncrementedIfNonEmpty) {
  bitset_.set(2);

  auto it = make_begin();
  ++it;
  ASSERT_NE(make_begin(), it);
}

TEST_F(DynamicBitsetIteratorTest, PostIncrementWorks) {
  bitset_.set(2);
  bitset_.set(6);

  auto it1 = make_begin();
  auto it2 = make_begin();
  EXPECT_EQ(it1, it2);

  EXPECT_EQ(it1, it2++);
  ++it1;
  EXPECT_EQ(it1, it2);

  EXPECT_EQ(it1, it2++);
  ++it1;
  EXPECT_EQ(it1, it2);
}

TEST_F(DynamicBitsetIteratorTest, BeginEqualsEndIfBitsetIsEmpty) {
  ASSERT_EQ(make_begin(), make_end());
}

TEST_F(DynamicBitsetIteratorTest, BeginNotEqualsEndIfBitsetIsNonEmpty) {
  bitset_.set(3);
  ASSERT_NE(make_begin(), make_end());
}

TEST_F(DynamicBitsetIteratorTest, DereferenceBegin) {
  bitset_.set(4);
  auto it = make_begin();
  ASSERT_EQ(4, *it);
}

TEST_F(DynamicBitsetIteratorTest, IteratorIsCorrectAfterIncrement) {
  bitset_.set(8);
  auto it = make_begin();
  ++it;
  ASSERT_EQ(make_end(), it);
}

TEST_F(DynamicBitsetIteratorTest, CopyWorks) {
  bitset_.set(3);
  bitset_.set(4);
  bitset_.set(7);

  std::vector<std::size_t> actual;
  std::copy(make_begin(), make_end(), std::back_inserter(actual));

  std::vector<std::size_t> expected = {3, 4, 7};
  ASSERT_EQ(expected, actual);
}

}
