/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <gtest/gtest.h>

#include "Line.hpp"

namespace kriss_kross {

TEST(LineTest, EmptyVectorGivesNoLines) {
  std::unique_ptr<Lines> lines = Lines::from_vector({});
  ASSERT_EQ(lines->begin(), lines->end());
}

TEST(LineTest, LinesHaveSameFilenameAsContainer) {
  std::unique_ptr<Lines> lines = Lines::from_vector({"A", "B", "C"});
  for (const Line& line : *lines) {
    ASSERT_EQ(lines->filename(), line.filename());
  }
}

TEST(LineTest, LinesHaveCorrectStrings) {
  std::vector<std::string> expected = {"FOO", "BAR", "FOOBAR"};
  std::unique_ptr<Lines> lines = Lines::from_vector(expected);

  std::vector<std::string> actual;
  for (const Line& line : *lines) {
    actual.push_back(line.string());
  }

  ASSERT_EQ(expected, actual);
}

TEST(LineTest, LinesHaveCorrectNumbers) {
  std::unique_ptr<Lines> lines = Lines::from_vector({
      "Donald Duck",
      "Scrooge McDuck",
      "Huey",
      "Dewey",
      "Louie"
  });

  std::vector<int> actual;
  for (const Line& line : *lines) {
    actual.push_back(line.number());
  }

  std::vector<int> expected = {1, 2, 3, 4, 5};
  ASSERT_EQ(expected, actual);
}

}
