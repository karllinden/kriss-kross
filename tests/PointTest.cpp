/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <gtest/gtest.h>

#include "Point.hpp"

namespace kriss_kross {

TEST(PointTest, DefaultPointIsOrigin) {
    ASSERT_EQ(Point(0, 0), Point());
}

TEST(PointTest, Point13HasCorrectX) {
    ASSERT_EQ(1, Point(1, 3).x());
}

TEST(PointTest, Point13HasCorrectY) {
    ASSERT_EQ(3, Point(1, 3).y());
}

TEST(PointTest, InPlacePlusWorks) {
    Point pt(6, 6);
    pt += Vector(-2, 2);
    ASSERT_EQ(Point(4, 8), pt);
}

TEST(PointTest, PlusWorks) {
    ASSERT_EQ(Point(3, 5), Point(2, 3) + Vector(1, 2));
}

TEST(PointTest, MinusWorks) {
    ASSERT_EQ(Vector(2, 1), Point(4, 4) - Point(2, 3));
}

TEST(PointTest, Point14IsLessThanOrEqualTo14) {
    ASSERT_TRUE(Point(1, 4) <= Point(1, 4));
}

TEST(PointTest, Point14IsNotLessThanOrEqualTo04) {
    ASSERT_FALSE(Point(1, 4) <= Point(0, 4));
}

TEST(PointTest, Point14IsNotLessThanOrEqualTo13) {
    ASSERT_FALSE(Point(1, 4) <= Point(1, 3));
}

TEST(PointTest, Point14IsLessThanOrEqualTo15) {
    ASSERT_TRUE(Point(1, 4) <= Point(1, 5));
}

TEST(PointTest, Point14IsLessThanOrEqualTo24) {
    ASSERT_TRUE(Point(1, 4) <= Point(2, 4));
}

TEST(PointTest, Point14IsLessThanOrEqualTo25) {
    ASSERT_TRUE(Point(1, 4) <= Point(2, 5));
}

TEST(PointTest, Point14IsNotLessThan14) {
    ASSERT_FALSE(Point(1, 4) < Point(1, 4));
}

TEST(PointTest, Point14IsNotLessThan04) {
    ASSERT_FALSE(Point(1, 4) < Point(0, 4));
}

TEST(PointTest, Point14IsNotLessThan13) {
    ASSERT_FALSE(Point(1, 4) < Point(1, 3));
}

TEST(PointTest, Point14IsNotLessThan15) {
    ASSERT_FALSE(Point(1, 4) < Point(1, 5));
}

TEST(PointTest, Point14IsNotLessThan24) {
    ASSERT_FALSE(Point(1, 4) < Point(2, 4));
}

TEST(PointTest, Point14IsLessThan25) {
    ASSERT_TRUE(Point(1, 4) < Point(2, 5));
}

TEST(PointTest, MinWorks) {
    ASSERT_EQ(Point(1, -1), min(Point(3, -1), Point(1, 3)));
}

TEST(PointTest, MaxWorks) {
    ASSERT_EQ(Point(2, 4), max(Point(0, 4), Point(2, 2)));
}

}
