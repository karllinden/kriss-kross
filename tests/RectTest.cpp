/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <gtest/gtest.h>

#include "Rect.hpp"

namespace kriss_kross {

class RectTest : public ::testing::Test {

protected:
    RectTest()
        : rect0_()
        , rect1245_(Point(1, 2), Point(4, 5))
        , rect4215_(Point(4, 2), Point(1, 5))
    {
        //
    }

    Rect rect0_;
    Rect rect1245_;
    Rect rect4215_;

};

TEST_F(RectTest, DefaultRectHasAllZeroes) {
    ASSERT_EQ(Rect(Point(0, 0), Point(0, 0)), rect0_);
}

TEST_F(RectTest, Rect1245EqualsRect4215) {
    ASSERT_EQ(rect1245_, rect4215_);
}

TEST_F(RectTest, Rect1245IsNotEqualToRect0245) {
    ASSERT_NE(Rect(Point(0, 2), Point(4, 5)), rect1245_);
}

TEST_F(RectTest, Rect1245IsNotEqualToRect1345) {
    ASSERT_NE(Rect(Point(1, 3), Point(4, 5)), rect1245_);
}

TEST_F(RectTest, Rect1245IsNotEqualToRect1244) {
    ASSERT_NE(Rect(Point(1, 2), Point(4, 4)), rect1245_);
}

TEST_F(RectTest, Rect1245IsNotEqualToRect1235) {
    ASSERT_NE(Rect(Point(1, 2), Point(3, 5)), rect1245_);
}

TEST_F(RectTest, DefaultRectHasTopLeft00) {
    ASSERT_EQ(Point(0, 0), rect0_.top_left());
}

TEST_F(RectTest, Rect1245HasTopLeft12) {
    ASSERT_EQ(Point(1, 2), rect1245_.top_left());
}

TEST_F(RectTest, Rect4215HasTopLeft12) {
    ASSERT_EQ(Point(1, 2), rect4215_.top_left());
}

TEST_F(RectTest, DefaultRectDoesNotContain00) {
    ASSERT_FALSE(rect0_.contains(Point(0, 0)));
}

TEST_F(RectTest, Rect1245Contains12) {
    ASSERT_TRUE(rect1245_.contains(Point(1, 2)));
}

TEST_F(RectTest, Rect1245DoesNotContain02) {
    ASSERT_FALSE(rect1245_.contains(Point(0, 2)));
}

TEST_F(RectTest, Rect1245DoesNotContain11) {
    ASSERT_FALSE(rect1245_.contains(Point(0, 1)));
}

TEST_F(RectTest, Rect1245DoesNotContain15) {
    ASSERT_FALSE(rect1245_.contains(Point(1, 5)));
}

TEST_F(RectTest, Rect1245DoesNotContain42) {
    ASSERT_FALSE(rect1245_.contains(Point(4, 2)));
}

TEST_F(RectTest, Rect1245Contains34) {
    ASSERT_TRUE(rect1245_.contains(Point(3, 4)));
}

TEST_F(RectTest, Rect1245DoesNotContain45) {
    ASSERT_FALSE(rect1245_.contains(Point(4, 5)));
}

TEST_F(RectTest, Rect1245HasSize9) {
    ASSERT_EQ(9, rect1245_.size());
}

TEST_F(RectTest, Rect1245HasWidth3) {
    ASSERT_EQ(3, rect1245_.width());
}

TEST_F(RectTest, PointsIn1245HasCorrectIndices) {
    std::array<Point, 9> points = {
        Point(1, 2),
        Point(2, 2),
        Point(3, 2),
        Point(1, 3),
        Point(2, 3),
        Point(3, 3),
        Point(1, 4),
        Point(2, 4),
        Point(3, 4)
    };

    for (int i = 0; i < 9; ++i) {
        EXPECT_EQ(i, rect1245_.index_of(points[i]));
    }
}

}
