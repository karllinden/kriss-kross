/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <string>

#include <gtest/gtest.h>

#include "SortedObjects.hpp"

using std::size_t;
using std::string;
using std::vector;

namespace kriss_kross {

struct Foo {

public:
  Foo(std::string string)
    : string_(std::move(string))
  {
    //
  }

  size_t length() const noexcept {
    return string_.length();
  }

  const std::string string() const noexcept {
    return string_;
  }

  bool operator<(const Foo& that) const noexcept {
    return string_ < that.string_;
  }

  size_t length_index() const noexcept {
    return length_index_;
  }

  void set_length_index(size_t index) noexcept {
    length_index_ = index;
  }

private:
  std::string string_;
  size_t length_index_;

};

using Foos = SortedObjects<Foo>;

template<typename Container>
vector<string> to_strings(const Container& foos) {
  vector<string> result;
  for (const Foo& f : foos) {
    result.push_back(f.string());
  }
  return result;
}

class SortedObjectsTest : public ::testing::Test {

protected:
  SortedObjectsTest()
    : one_("one")
    , two_("two")
    , three_("three")
    , four_("four")
    , five_("five")
    , six_("six")
    , seven_("seven")
    , eight_("eight")
    , nine_("nine")
    , ten_("ten")
    , twelve_("twelve")
    , one_to_ten_({
        one_,
        two_,
        three_,
        four_,
        five_,
        six_,
        seven_,
        eight_,
        nine_,
        ten_})
  {
    //
  }

protected:
  Foo one_;
  Foo two_;
  Foo three_;
  Foo four_;
  Foo five_;
  Foo six_;
  Foo seven_;
  Foo eight_;
  Foo nine_;
  Foo ten_;
  Foo twelve_;

  Foos one_to_ten_;

};

TEST_F(SortedObjectsTest, ObjectsAreSortedByLengthFirst) {
  Foos foos({four_, two_, twelve_, eight_});
  vector<string> actual = to_strings(foos);
  vector<string> expected = {"two", "four", "eight", "twelve"};
  ASSERT_EQ(expected, actual);
}

TEST_F(SortedObjectsTest, ObjectsAreSortedDefaultSecondly) {
  Foos foos({four_, nine_, five_});
  vector<string> actual = to_strings(foos);
  vector<string> expected = {"five", "four", "nine"};
  ASSERT_EQ(expected, actual);
}

TEST_F(SortedObjectsTest, SizeIsCorrect) {
  EXPECT_EQ(3, Foos({two_, four_, five_}).size());
  EXPECT_EQ(4, Foos({four_, five_, eight_, nine_}).size());
}

TEST_F(SortedObjectsTest, MaxLengthIsCorrect) {
  EXPECT_EQ(0, Foos({}).max_length());
  EXPECT_EQ(4, Foos({two_, four_}).max_length());
  EXPECT_EQ(5, one_to_ten_.max_length());
}

TEST_F(SortedObjectsTest, WithLengthHaveCorrectLength) {
  Foos foos({two_, four_, five_, twelve_});
  for (int i = 0; i < 6; ++i) {
    EXPECT_EQ(i, foos.with_length(i).length());
  }
}

TEST_F(SortedObjectsTest, WithLengthHaveCorrectElements) {
  vector<string> empty;
  vector<string> actual3 = {"one", "six", "ten", "two"};
  vector<string> actual4 = {"five", "four", "nine"};
  vector<string> actual5 = {"eight", "seven", "three"};
  EXPECT_EQ(empty, to_strings(one_to_ten_.with_length(0)));
  EXPECT_EQ(empty, to_strings(one_to_ten_.with_length(1)));
  EXPECT_EQ(empty, to_strings(one_to_ten_.with_length(2)));
  EXPECT_EQ(actual3, to_strings(one_to_ten_.with_length(3)));
  EXPECT_EQ(actual4, to_strings(one_to_ten_.with_length(4)));
  EXPECT_EQ(actual5, to_strings(one_to_ten_.with_length(5)));
  EXPECT_EQ(empty, to_strings(one_to_ten_.with_length(6)));
  EXPECT_EQ(empty, to_strings(one_to_ten_.with_length(7)));
}

TEST_F(SortedObjectsTest, WithLengthSizesAreCorrect) {
  EXPECT_EQ(0, one_to_ten_.with_length(0).size());
  EXPECT_EQ(0, one_to_ten_.with_length(1).size());
  EXPECT_EQ(0, one_to_ten_.with_length(2).size());
  EXPECT_EQ(4, one_to_ten_.with_length(3).size());
  EXPECT_EQ(3, one_to_ten_.with_length(4).size());
  EXPECT_EQ(3, one_to_ten_.with_length(5).size());
  EXPECT_EQ(0, one_to_ten_.with_length(6).size());
  EXPECT_EQ(0, one_to_ten_.with_length(7).size());
}

TEST_F(SortedObjectsTest, LengthIndicesAreCorrect) {
  vector<size_t> actual;
  for (const Foo& foo : one_to_ten_) {
    actual.push_back(foo.length_index());
  }
  vector<size_t> expected = {0, 1, 2, 3, 0, 1, 2, 0, 1, 2};
  ASSERT_EQ(expected, actual);
}

}
