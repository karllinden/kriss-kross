/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <gtest/gtest.h>

#include "Vector.hpp"

namespace kriss_kross {

class VectorTest : public ::testing::Test {

protected:
    VectorTest()
        : vec12_(1, 2)
        , vec34_(3, 4)
    {
        //
    }

    Vector vec12_;
    Vector vec34_;

};

TEST_F(VectorTest, Vector12HasCorrectX) {
    ASSERT_EQ(1, vec12_.x);
}

TEST_F(VectorTest, Vector12HasCorrectY) {
    ASSERT_EQ(2, vec12_.y);
}

TEST_F(VectorTest, VectorsWithSameCoordinatesAreEqual) {
    ASSERT_EQ(Vector(1, 2), vec12_);
}

TEST_F(VectorTest, VectorsWithDifferentXAreNotEqual) {
    ASSERT_NE(Vector(0, 2), vec12_);
}

TEST_F(VectorTest, VectorsWithDifferentYAreNotEqual) {
    ASSERT_NE(Vector(1, 3), vec12_);
}

TEST_F(VectorTest, InPlacePlusWorks) {
    Vector vec(1, 2);
    vec += vec34_;
    ASSERT_EQ(Vector(4, 6), vec);
}

TEST_F(VectorTest, PlusWorks) {
    ASSERT_EQ(Vector(4, 6), vec12_ + vec34_);
}

TEST_F(VectorTest, InPlaceMinusWorks) {
    Vector vec(4, 4);
    vec -= vec34_;
    ASSERT_EQ(Vector(1, 0), vec);
}

TEST_F(VectorTest, MinusWorks) {
    ASSERT_EQ(Vector(2, 2), vec34_ - vec12_);
}

TEST_F(VectorTest, Vector12IsLessThanOrEqualToVector12) {
    ASSERT_TRUE(vec12_ <= Vector(1, 2));
}

TEST_F(VectorTest, Vector12IsLessThanOrEqualToVector13) {
    ASSERT_TRUE(vec12_ <= Vector(1, 3));
}

TEST_F(VectorTest, Vector13IsNotLessThanOrEqualToVector12) {
    ASSERT_FALSE(Vector(1, 3) <= vec12_);
}

TEST_F(VectorTest, Vector12IsLessThanOrEqualToVector22) {
    ASSERT_TRUE(vec12_ <= Vector(2, 2));
}

TEST_F(VectorTest, Vector22IsNotLessThanOrEqualToVector12) {
    ASSERT_FALSE(Vector(2, 2) <= vec12_);
}

TEST_F(VectorTest, Vector12IsLessThanOrEqualToVector34) {
    ASSERT_TRUE(vec12_ <= Vector(3, 4));
}

TEST_F(VectorTest, Vector12IsNotLessThanVector12) {
    ASSERT_FALSE(vec12_ < Vector(1, 2));
}

TEST_F(VectorTest, Vector12IsNotLessThanVector13) {
    ASSERT_FALSE(vec12_ < Vector(1, 3));
}

TEST_F(VectorTest, Vector13IsNotLessVector12) {
    ASSERT_FALSE(Vector(1, 3) < vec12_);
}

TEST_F(VectorTest, Vector12IsNotLessThanVector22) {
    ASSERT_FALSE(vec12_ < Vector(2, 2));
}

TEST_F(VectorTest, Vector22IsNotLessVector12) {
    ASSERT_FALSE(Vector(2, 2) < vec12_);
}

TEST_F(VectorTest, Vector12IsLessThanVector34) {
    ASSERT_TRUE(vec12_ < Vector(3, 4));
}

TEST_F(VectorTest, MinWorks) {
    ASSERT_EQ(Vector(4, 2), min(Vector(4, 5), Vector(5, 2)));
}

TEST_F(VectorTest, MaxWorks) {
    ASSERT_EQ(Vector(5, 5), max(Vector(4, 5), Vector(5, 2)));
}

}
