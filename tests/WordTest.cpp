/*
 * This file is part of kriss-kross.
 *
 * Copyright (C) 2020 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <gtest/gtest.h>

#include "Word.hpp"

using std::size_t;
using std::string;
using std::vector;

namespace kriss_kross {

vector<size_t> to_counts(const Words::WithLength& words) {
  vector<size_t> result;
  for (const Word& word : words) {
    result.push_back(word.count());
  }
  return result;
}

vector<string> to_strings(const Words::WithLength& words) {
  vector<string> result;
  for (const Word& word : words) {
    result.push_back(word.string());
  }
  return result;
}

TEST(WordTest, StringIsCorrect) {
  Word banana = Word::known("BANANA");
  Word orange = Word::known("ORANGE");
  ASSERT_EQ("BANANA", banana.string());
  ASSERT_EQ("ORANGE", orange.string());
}

TEST(WordTest, LengthIsCorrect) {
  Word money = Word::known("MONEY");
  Word invoice = Word::known("INVOICE");
  ASSERT_EQ(5, money.length());
  ASSERT_EQ(7, invoice.length());
}

TEST(WordTest, DoesNotFitShorterString) {
  Word word = Word::known("TOMATO");
  ASSERT_FALSE(word.fits("???"));
}

TEST(WordTest, DoesNotFitLongerString) {
  Word word = Word::known("PINEAPPLE");
  ASSERT_FALSE(word.fits("PINEAPPLES"));
}

TEST(WordTest, FitsSelf) {
  Word word = Word::known("CUCUMBER");
  ASSERT_TRUE(word.fits("CUCUMBER"));
}

TEST(WordTest, FitsUnknown) {
  Word word = Word::known("DATE");
  ASSERT_TRUE(word.fits("????"));
}

TEST(WordTest, DoesNotFitIfCharacterIsDifferent) {
  Word word = Word::known("CARROT");
  ASSERT_FALSE(word.fits("??D???"));
}

TEST(WordTest, UnknownDoesNotFitDifferentLength) {
  Word word = Word::unknown(3);
  ASSERT_FALSE(word.fits("????"));
}

TEST(WordTest, UnknownFitsUnknown) {
  Word word = Word::unknown(4);
  ASSERT_TRUE(word.fits("????"));
}

TEST(WordTest, UnknownFitsKnown) {
  Word word = Word::unknown(5);
  ASSERT_TRUE(word.fits("GRAAL"));
}

TEST(WordTest, CreateEmptyMapWorks) {
  WordMap map = WordMap::from_known_words({});
  auto wwl0 = map.words_with_length(0);
  EXPECT_EQ(0, wwl0.size());
  EXPECT_EQ(wwl0.begin(), wwl0.end());
}

TEST(WordTest, EmptyWordsAreIgnored) {
  WordMap map = WordMap::from_known_words({"", ""});
  auto wwl0 = map.words_with_length(0);
  EXPECT_EQ(0, wwl0.size());
  EXPECT_EQ(wwl0.begin(), wwl0.end());
}

TEST(WordTest, ThrowsIfWordContainsSpace) {
  ASSERT_THROW({ WordMap::from_known_words({"FOO BAR"}); },
      std::exception);
}

TEST(WordTest, ThrowsIfWordContainsQuestionMark) {
  ASSERT_THROW({ WordMap::from_known_words({"MUSHR?OM"}); },
      std::exception);
}

TEST(WordTest, ThrowsIfWordHasOnlyOneCharacter) {
  ASSERT_THROW({ WordMap::from_known_words({"I"}); }, std::exception);
}

TEST(WordTest, WordsOfSameLengthAreSortedLexically) {
  WordMap map = WordMap::from_known_words(
      {"TOES", "NOSE", "EARS", "TOMB"});

  vector<string> actual = to_strings(map.words_with_length(4));

  vector<string> expected{"EARS","NOSE", "TOES", "TOMB"};
  ASSERT_EQ(expected, actual);
}

TEST(WordTest, HasCorrectCounts) {
  WordMap map = WordMap::from_known_words({
      "TIME",
      "ONCE",
      "UPON",
      "UPON",
      "TIME",
      "TIME"});

  vector<size_t> actual = to_counts(map.words_with_length(4));
  vector<size_t> expected{1, 3, 2};
  ASSERT_EQ(expected, actual);
}

TEST(WordTest, HasCorrectWithLengthCount) {
  WordMap map = WordMap::from_known_words({
      "AB",
      "CD",
      "EFG",
      "EFG",
      "HIJ"});

  Counts expected({0, 0, 2, 3});
  EXPECT_EQ(expected, map.count_words());
}

void test_combine_strings(size_t length,
    const vector<string>& first,
    const vector<string>& second,
    const vector<string>& expected) {
  WordMap first_map = WordMap::from_known_words(first);
  WordMap second_map = WordMap::from_known_words(second);
  WordMap combined_map = WordMap::combine(first_map, second_map);

  vector<string> actual = to_strings(
      combined_map.words_with_length(length));
  ASSERT_EQ(expected, actual);
}

void test_combine_counts(size_t length,
    const vector<string>& first,
    const vector<string>& second,
    const vector<size_t>& expected) {
  WordMap first_map = WordMap::from_known_words(first);
  WordMap second_map = WordMap::from_known_words(second);
  WordMap combined_map = WordMap::combine(first_map, second_map);

  vector<size_t> actual = to_counts(
      combined_map.words_with_length(length));
  ASSERT_EQ(expected, actual);
}

TEST(WordTest, CombineProducesCorrectStringWithDuplicate) {
  test_combine_strings(
      3,
      {"CUP", "TEA"},
      {"CAP", "TEA"},
      {"CAP", "CUP", "TEA"});
}

TEST(WordTest, CombineProducesCorrectStringsWithUnique) {
  test_combine_strings(
      4,
      {"CURE", "TEAR"},
      {"FUSE", "GORE"},
      {"CURE", "FUSE", "GORE", "TEAR"});
}

TEST(WordTest, CombineProducesCorrectCountsWithDuplicates) {
  vector<size_t> expected{
    1, // DRUGS
    2, // FLIRT
    1, // MAGIC
    2, // SMART
    3, // START
    1, // WRATH
  };
  test_combine_counts(
      5,
      {"FLIRT", "FLIRT", "MAGIC", "SMART", "START", "WRATH"},
      {"DRUGS", "SMART", "START", "START"},
      expected);
}

TEST(WordTest, UnknownsProduceCorrectResult) {
  Counts counts({0, 2, 1, 0, 3, 4});
  WordMap map = WordMap::unknowns(counts);
  EXPECT_EQ(counts, map.count_words());

  for (size_t len = 0; len <= counts.max_non_zero_length(); ++len) {
    Words::WithLength wwl = map.words_with_length(len);
    if (counts[len] != 0) {
      EXPECT_EQ(1, wwl.size());
      EXPECT_TRUE(wwl[0].is_unknown());
    } else {
      EXPECT_EQ(0, wwl.size());
    }
  }
}

}

